# Divergences

## Major Features

- quote posting
- Treehouse::Automod (experimental feature flagged)
- TH_MAILER_SIDEKIQ_RETRY_LIMIT=2

## Other Changes

- various build system changes
  - a better dockerfile
  - yarn v2 (a mistake, tbh)
  - various dev env changes
- various css/style changes
